<?php
require_once("config.php");
require_once("logs.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_GET["getCurrentBilling"])){
    $id = $_GET["getCurrentBilling"];

    $database->where ("franchise_id", $loggedUser->franchisee_id);
    $database->where("status", "UNPAID");
    $database->where("is_deleted", 0);
    $userDB = $database->get(BILLING_FRANCHISING);
    echo json_encode($userDB);
}

?>