<?php
require_once("config.php");
require_once("logs.php");

function getLoggedUserDetails($database){
    $username = $_COOKIE["username-franchise"];

    $database->where("username", $username);
    $userDB = $database->getOne(ACCOUNTS);
    return json_encode($userDB);
}

function setLoggedUser($username){
    setcookie('username-franchise', $username , time() + 86400, "/");
}

if(isset($_GET["getLoggedUserDetails"])){
    echo getLoggedUserDetails($database);
}

if(isset($_POST["data"]) && preg_match('/auth.php/', $actual_link)) {
    $postData = json_decode($_POST["data"]);
    $action = $postData->action;
    $data = json_decode($postData->data);

    if($action == "login"){
        $database->where("is_deleted", 0);
        $database->where ("username", $data->username);
        $userDB = $database->getOne (ACCOUNTS);

        if (empty($userDB)) {
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => "You do not have an account to access this system"
            ));
            return;
        } 

        if($userDB['username'] == $data->username && password_verify($data->password,$userDB['password'])){
            $franchise = getFranchise($userDB["franchisee_id"]);

            if ($franchise["date_contract_expiry"] == date("Y-m-d")) {
                echo json_encode(Array (
                    "type" => "error",
                    "title" => "Error",
                    "text" => "Your contract is already expired"
                ));
            } else {
                
                echo json_encode(Array (
                    "type" => "success",
                    "title" => "Successful!",
                    "text" => "Logged in succesfully!"
                ));
                setLoggedUser($userDB['username']);
                saveLog($database,"Logged in");
            }
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => "Wrong username or password!"
            ));
        }
    }
}



?>