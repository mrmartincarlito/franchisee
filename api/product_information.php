<?php
require_once("config.php");
require_once("logs.php");

//GET METHODS
if(isset($_GET["get"])){
    $primaryKey = 'id';
    $columns = array(
        array(  'db' => 
                'image_file',  'dt' => 0 ,
                'formatter' => function ($data, $row){
                    $image = empty($data) ? "plugins/images/no-item.png" : URL . "accounting/api/$data";
                    return '<img src="'.$image.'" alt="NO IMAGE" class="img-thumbnail">';

                }
        ),
        array(  'db' => 'description',   
                'dt' => 1 ,
                'formatter' => function ($data, $row){ 
                   
                    return "<h3>".$data."</h3>";
                } 
        ),
        array( 'db' => 'id',   
                'dt' => 2, 
                'formatter' => function ($data, $row){ 
                    $product = getProduct($data);
                    
                    $select = "<select class='select2' id='qty_choice_".$data."' style='width:50%'>";

                    for ($i = 1; $i<=2000; $i++) {
                        $select .= "<option>".($i * $product["order_count"])."</option>";
                    }

                    $select .= "</select>";

                    $cart = '<button type="button" id="cart_'.$data.'" class="btn btn-danger" onclick="addToCart('.$data.')"><i class="fa fa-cart-plus m-r-5"></i> <span>ADD TO CART</span></button>';

                    return "<h3>Qty: $select</h3>
                    <h3>Price: <b>Php ".number_format($product["price"])."</b></h3>
                    $cart"; 
                } 
        )
    );
    
    $condition = "is_deleted = 0 and NOT (category = 'OTHERS')";

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, PRODUCTS , $primaryKey, $columns, $condition )
    );
}

?>