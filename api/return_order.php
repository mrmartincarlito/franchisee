<?php
require_once("config.php");
require_once("logs.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_GET["get"])){

    $database->where("is_deleted", 0);
    $database->where ("franchisee_id", $loggedUser->franchisee_id);
    $userDB = $database->get(ORDER_RETURNS);
    echo json_encode($userDB);
}

if(isset($_POST['proceedReturn'])){
    $data = json_decode($_POST['proceedReturn']);

    $refNo = generateRefno($database, "1", ORDER_RETURNS, "return_ref_no", "R-");

    $database->where("is_deleted", 0);
    $database->where("order_ref", $data->info->order_ref_no);
    $order = $database->getOne(ORDERS);

    if($order["status"] == "APPROVED" || $order["status"] == "PENDING"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "This order is not yet received by the franchisee"
        ));

        return;
    }

    $id = $database->insert(ORDER_RETURNS, array(
        "order_ref_no" => $data->info->order_ref_no,
        "return_ref_no" => $refNo,
        "franchisee_id" => $loggedUser->franchisee_id,
        "status" => "PENDING",
        "added_by" => $loggedUser->username,
        "remarks" => $data->info->remarks
    ));

    if($id){
        foreach($data->orders as $order){
            $database->insert(ORDER_RETURN_ITEMS, array(
                "return_ref" => $refNo,
                "item_id" => getOrderItems($order->id)["item_id"],
                "return_qty" => $order->qty,
                "cost" => $order->price,
                "total_amount" => $order->total_amount
            ));
        }

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Successfully Placed Your Return Goods!"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "Something went wrong please try again later"
        ));
    }
}

if(isset($_POST['cancelReturn'])){
    $id = $_POST['cancelReturn'];

    $database->where("id", $id);
    $return = $database->getOne(ORDER_RETURNS);

    if($return["status"] == "APPROVED"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "This is already APPROVED by the commissary, please contact them to cancel"
        ));

        return;
    }

    if($return["status"] == "REJECTED"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "You cannot cancel a REJECTED return request"
        ));

        return;
    }

    $database->where("id", $id);
    $id = $database->update(ORDER_RETURNS, array("is_deleted" => 1, "status" => "CANCELLED"));

    if($id){
        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "You just cancelled your return request"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "Something went wrong please try again later"
        ));
    }

}