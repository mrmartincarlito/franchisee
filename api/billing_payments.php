<?php
require_once("config.php");
require_once("logs.php");
require_once("payments-movements.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    $billing = explode("|", $data->description);

    if($data->formAction == "add"){

        //check if there is pending bills payment in the current billing
        $database->where("bill_id", $billing[0]);
        $database->where("franchise_id", $loggedUser->franchisee_id);
        $database->where("status", "PENDING");
        $checkIfPending = $database->getOne(BILLING_PAYMENTS);

        if(!empty($checkIfPending)){
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => "You have Pending bills payment in this billing"
            ));
            return;
        }

        $insertData = Array (
            "franchise_id" => $loggedUser->franchisee_id,
            "bill_id" => $billing[0],
            "description" => $billing[1],
            "total_amount" => $data->total_amount,
            "date_paid" => $database->now(),
            "check_no" => isset($data->check_no) ? $data->check_no : "",
            "check_details" => isset($data->check_details) ? $data->check_details : "",
            "bank" => isset($data->bank) ? $data->bank : "",
            "remarks" => isset($data->remarks) ? $data->remarks : "",
            "status" => "PENDING",
        );

        $id = $database->insert (BILLING_PAYMENTS, $insertData);
        if($id){
            $_SESSION['image-billing-payment-id'] = $id;

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Billing Payment Added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "franchise_id" => $loggedUser->franchisee_id,
            "bill_id" => $billing[0],
            "description" => $billing[1],
            "total_amount" => $data->total_amount,
            "date_paid" => $database->now(),
            "check_no" => isset($data->check_no) ? $data->check_no : "",
            "check_details" => isset($data->check_details) ? $data->check_details : "",
            "bank" => isset($data->bank) ? $data->bank : "",
            "remarks" => isset($data->remarks) ? $data->remarks : "",
            "status" => "PENDING",
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (BILLING_PAYMENTS, $updateData);
        if($id){

            $_SESSION['image-billing-payment-id'] = $data->modifyId;

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Billing Payment Details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (BILLING_PAYMENTS, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Billing Payment Deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} Billing Payment: {$data->description}");
    }else{
        saveLog($database,"{$data->formAction} Billing Payment ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $primaryKey = 'id'; //primary key
    $columns = array(
        //db = column name
        //dt = 0
        //formatter =
        array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'description',  'dt' => 1 ),
        array(  'db' => 'total_amount',  
                 'dt' => 2 ,
                 'formatter' => function($data, $row ){
                     return number_format($data);
                 }
            ),
        array( 'db' => 'remarks',   'dt' => 3 ),
        array(  'db' => 'status',   
                'dt' => 4,
                'formatter' => function ($data, $row){

                    return convertStatusColor($data);
                }
            ),
        array( 'db' => 'or_number',   'dt' => 5 ),
        array( 'db' => 'invoice_number',   'dt' => 6 ),
        array(  'db' => 'id',   
                'dt' => 7 ,
                'formatter' => function($data ,$row) {

                    if($row["status"] == "PENDING"){
                        return ' <div class="btn-group dropdown">
                                <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                                <ul role="menu" class="dropdown-menu animated">
                                    <li><a href="#" onclick="modify('.$data.')">Modify</a></li>
                                    <li><a href="#" onclick="viewImage('.$data.')">View Image Receipt</a></li>
                                </ul>
                            </div>';
                    }else{
                        return ' <div class="btn-group dropdown">
                                <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                                <ul role="menu" class="dropdown-menu animated">
                                    <li><a href="#" onclick="viewImage('.$data.')">View Image Receipt</a></li>
                                </ul>
                            </div>';
                    } 
                }
            ),
    );
    
    $condition = "is_deleted = 0 and franchise_id = " .$loggedUser->franchisee_id;

    if(isset($_GET['status'])){
        $condition .= " and status = '{$_GET['status']}'";
    }

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, BILLING_PAYMENTS , $primaryKey, $columns, $condition )
    );
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $userDB = $database->getOne(BILLING_PAYMENTS);
    echo json_encode($userDB);
}

?>