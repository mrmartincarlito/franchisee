<?php

include "env.php";
include "env.".ENV.".php";

//Tables
define("ACCOUNTS", "franchise_accounts");
define("LOGS", "logs");
define("FRANCHISE", "franchisee");
define("BILLING_PAYMENTS", "billing_payments");
define("ORDER_PAYMENTS", "order_payments");
define("BRANCHES", "franchise_branch");
define("PRODUCTS","products");
define("SUPPLIER", "supplier");
define("BILLING_FRANCHISING", "billing_franchise");
define("ORDERS", "orders");
define("ORDER_ITEMS", "order_items");
define("ORDER_RETURNS", "order_returns");
define("ORDER_RETURN_ITEMS","order_return_items");


function generateRefno($database, $initialCount, $table, $columnRef, $pattern){
    $data = $database->get($table);

    if(!empty($data)){
        $database->orderBy("id","DESC");
        $fetchedData = $database->getOne($table);

        $columnValue = $fetchedData[$columnRef];
        $toIncrement = substr($columnValue, 2);
        $toIncrement = $toIncrement + 1;

        return $pattern.str_pad($toIncrement, 4, '0', STR_PAD_LEFT);
    }else{
        return $pattern.str_pad($initialCount, 4, '0', STR_PAD_LEFT);
    }
    
}

function convertStatusColor($data){
    $color = "";
    if($data == "APPROVED" || $data == "PAID"){
        $color = '<span class="label label-success">' . $data . '</span>';
    }

    if ($data == "DISPOSED") {
        $color = '<span class="label label-info">' . $data . '</span>';
    }

    if($data == "REJECTED" || $data == "UNPAID" || $data == "CANCELLED"){
        $color = '<span class="label label-danger">' . $data . '</span>';
    }

    if($data == "PENDING"){
        $color = '<span class="label label-info">' . $data . '</span>';
    }

    if($data == "RECEIVED"){
        $color = '<span class="label label-warning">' . $data . '</span>';
    }

    if($data == "Delivery" || $data == "Pick-up" || strpos($data, "Partial") !== false){
        $color = '<span class="label label-info">' . $data . '</span>';
    }

    return $color;
}

function getSupplier($id){
    global $database;

    $database->where ("id", $id);
    $supplier = $database->getOne(SUPPLIER);
    return $supplier;
}

function getProduct($id){
    global $database;

    $database->where ("id", $id);
    $products = $database->getOne(PRODUCTS);
    return $products;
}

function getFranchise($id){
    global $database;

    $database->where ("id", $id);
    $products = $database->getOne(FRANCHISE);
    return $products;
}

function getBranch($id){
    global $database;

    $database->where ("id", $id);
    $products = $database->getOne(BRANCHES);
    return $products;
}

function getOrderItems($id){
    global $database;

    $database->where ("id", $id);
    $products = $database->getOne(ORDER_ITEMS);
    return $products;
}