<?php
require_once("config.php");
require_once("logs.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_GET['itemsLookUp'])){
    $items = json_decode($_GET['itemsLookUp'] != "null" ? $_GET['itemsLookUp'] : "[]");

    $response = array();

    foreach($items as $item){
        $database->where("id", $item);
        $product = $database->getOne(PRODUCTS);

        array_push($response, $product);
    }

    echo json_encode($response);
}

if(isset($_GET['balanceLookUp'])){
    $franchisee = getFranchise($loggedUser->franchisee_id);

    $response["previous_balance"] = $franchisee["order_balance"];
    $response["franchisee_owner"] = $franchisee["name"];
    $response["status"] = $franchisee["order_balance"] > 0 ? "CAN NOT PROCEED" : "PENDING";
    $response["order_ref_no"] = "TMP-".generateRefno($database, "1", ORDERS, "order_ref", "O-") . "-" . $loggedUser->id;

    echo json_encode($response);
}

if(isset($_POST['saveOrder'])){
    $items = json_decode($_POST['saveOrder']);

    $total_amount = 0;
    $refNo = generateRefno($database, "1", ORDERS, "order_ref", "O-");

    foreach($items as $item){
        $total_amount = $total_amount + str_replace("," , "" , $item->total_amount); 
    }

    //check if there still balance
    $franchisee = getFranchise($loggedUser->franchisee_id);
    if($franchisee["order_balance"] > 0){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "Please settle your order balance before ordering again. Please call accounting office if needed to reorder again. Existing Balance: ".number_format($franchisee["order_balance"])
        ));

        return;
    }

    $franchiseeInfo = getFranchise($loggedUser->franchisee_id);

    $id = $database->insert(ORDERS, array(
        "order_ref" => $refNo,
        "franchisee_id" => $loggedUser->franchisee_id,
        "total_amount" => $total_amount,
        "added_by" => $loggedUser->username,
        "date_ordered" => $database->now(),
        "delivery_method" => $franchiseeInfo['mode_of_transaction'],
        //"delivery_charge" => $franchiseeInfo['delivery_charge']
    ));

    if($id){
        foreach($items as $item){
            $database->insert(ORDER_ITEMS, array(
                "order_id" => $id,
                "order_ref" => $refNo,
                "item_id" => $item->item_id,
                "price" => $item->price,
                "qty" => $item->qty,
                "total_amount" => $item->total_amount
            ));
        }

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Successfully Placed your order please track order status!"
        ));
    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }
}

if(isset($_GET["get"])){
    $primaryKey = 'id';
    $columns = array(
        array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'order_ref',  'dt' => 1 ),
        array( 'db' => 'total_amount',   'dt' => 2 , 'formatter' => function($data ,$row) { return number_format($data);}),
        array(  'db' => 'status',   
                'dt' => 3,
                'formatter' => function ($data, $row){

                    return convertStatusColor($data);
                }
            ),
        array( 'db' => 'added_by',   'dt' => 4 ),
        array( 'db' => 'date_ordered',   'dt' => 5 ),
        array(  'db' => 'id',   
                'dt' => 6 ,
                'formatter' => function($data ,$row) {
                    // $receiving  = "";

                    // if($row["status"] == "APPROVED"){
                    //     $receiving = '<li><a href="#" onclick="receiveItems('.$data.')">Receiving of Orders</a></li>';
                    // }

                    // return ' <div class="btn-group dropdown">
                    //             <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                    //             <ul role="menu" class="dropdown-menu animated">
                    //                 <li><a href="#" onclick="loadOrderItems('.$data.')">View Order</a></li>
                    //                 '.$receiving.'
                    //             </ul>
                    //         </div>';

                    return '<button type="button" class="btn btn-primary" onclick="loadOrderItems('.$data.')"><i class="fa fa-eye m-r-5"></i> <span>VIEW ORDER</span></button>';
                }
            ),
    );
    
    $condition = "is_deleted = 0 and franchisee_id = ".$loggedUser->franchisee_id;

    if(isset($_GET['status'])){
        $condition .= " and status = '{$_GET['status']}'";
    }

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, ORDERS , $primaryKey, $columns, $condition )
    );
}

if(isset($_POST['previewOrder'])){
    $items = json_decode($_POST['previewOrder']);

    $response = array();

    foreach($items as $item){
        $product = getProduct($item);

        array_push($response, $product);
    }

    echo json_encode($response);
}

if(isset($_GET['viewOrderedItems'])){
    $order_id = $_GET['viewOrderedItems'];

    if(isset($_GET['order_ref'])){
        $database->where("order_ref", $_GET['order_ref']);
    }else{
        $database->where("id", $order_id);
    }
    
    $orderInfo = $database->getOne(ORDERS);

    $database->where("order_id", $orderInfo["id"]);
    $orders = $database->get(ORDER_ITEMS);

    $response = array();

    foreach($orders as $item){
        $values["id"] = $item["id"];
        $values["item_id"] = $item["item_id"];
        $values["description"] = getProduct($item["item_id"])["description"];

        if($orderInfo["status"] == "RECEIVED"){
            $values["total_amount"] = $item["rec_total_amount"];
            $values["qty"] = $item["rec_qty"];
        }else{
            $values["total_amount"] = $item["total_amount"];
            $values["qty"] = $item["qty"];
        }
        
        $values["price"] = $item["price"];
        $values["order_ref_no"] = $item["order_ref"];

        array_push($response, $values);
    }

    echo json_encode($response);
}

if(isset($_GET['orderAgain'])){
    $order_ref = $_GET['orderAgain'];

    $refNo = generateRefno($database, "1", ORDERS, "order_ref", "O-");

    $database->where("order_ref", $order_ref);
    $order = $database->getOne(ORDERS);

    $database->where("order_ref", $order_ref);
    $orderItems = $database->get(ORDER_ITEMS);

    //check if there still balance
    $franchisee = getFranchise($order["franchisee_id"]);
    if($franchisee["order_balance"] > 0){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "Please settle your order balance before ordering again. Please call accounting office if needed to reorder again. Existing Balance: ".number_format($franchisee["order_balance"])
        ));

        return;
    }

    $id = $database->insert(ORDERS, array(
        "order_ref" => $refNo,
        "franchisee_id" => $order["franchisee_id"],
        "added_by" => $loggedUser->username,
        "status" => "PENDING",
        "total_amount" => $order["total_amount"],
        "date_ordered" => $database->now()
    ));

    if($id){
        foreach($orderItems as $item){
            $database->insert(ORDER_ITEMS, array(
                "order_id" => $id,
                "order_ref" => $refNo,
                "item_id" => $item["item_id"],
                "price" => $item["price"],
                "qty" => $item["qty"],
                "total_amount" => $item["total_amount"]
            ));
        }

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful",
            "text" => "Successfully placed your new order again"
        ));

    }else{
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => $database->getLastError()
        ));
    }
}

if(isset($_GET['cancelOrder'])){
    $order_ref = $_GET['cancelOrder'];

    $database->where("order_ref", $order_ref);
    $order = $database->getOne(ORDERS);

    if($order["status"] != "PENDING"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error!",
            "text" => "This order is already {$order["status"]}, Please contact the commissary or accounting for cancellation"
        ));

        return;
    }

    $database->where("order_ref", $order_ref);
    $id = $database->update(ORDERS, array(
        "is_deleted" => 1
    ));

    if($id){
        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "You just cancelled your order"
        ));
    }
}

if(isset($_POST['receiveOrder'])){
    $data = json_decode($_POST['receiveOrder']);

    $database->where("order_ref", $data->info->order_ref);
    $checkOrder = $database->getOne(ORDERS);

    if($checkOrder["status"] == "RECEIVED"){
        echo json_encode(Array (
            "type" => "error",
            "title" => "Error",
            "text" => "This order has already been received"
        ));

        return;
    }
   
    $database->where("order_ref", $data->info->order_ref);
    $id = $database->update(ORDERS, array(
        "status" => "RECEIVED",
        "rec_total_amount" => str_replace("," , "" , $data->info->overall_total),
        "remarks" => $data->info->remarks,
        "date_delivered" => $data->info->date_delivered
    ));

    if($id){
        foreach($data->orders as $order){
            $database->where("id", $order->id);
            $database->update(ORDER_ITEMS, array(
                "rec_qty" => $order->qty,
                "rec_total_amount" => $order->total_amount
            ));
        }

        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful",
            "text" => "Successfully received order!"
        ));

    }else{
        echo json_encode(Array (
            "type" => "success",
            "title" => "Successful!",
            "text" => "Something went while receiving please try again later"
        ));
    }
}