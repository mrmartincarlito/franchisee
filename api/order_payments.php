<?php
require_once("config.php");
require_once("logs.php");
require_once("payments-movements.php");

$loggedUser = json_decode(getLoggedUserDetails($database));

if(isset($_POST["data"])){
    $postData = json_decode($_POST["data"]);
    $data = json_decode($postData->data);

    if($data->formAction == "add"){

        $insertData = Array (
            "franchise_id" => $loggedUser->franchisee_id,
            "total_amount" => $data->total_amount,
            "date_paid" => $database->now(),
            "check_no" => isset($data->check_no) ? $data->check_no : "",
            "check_details" => isset($data->check_details) ? $data->check_details : "",
            "bank" => isset($data->bank) ? $data->bank : "",
            "remarks" => isset($data->remarks) ? $data->remarks : "",
            "status" => "PENDING",
        );

        $id = $database->insert (ORDER_PAYMENTS, $insertData);
        if($id){
            $_SESSION['image-order-payment-id'] = $id;

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Order Payment Added successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "edit"){

        $updateData = Array (
            "franchise_id" => $loggedUser->franchisee_id,
            "total_amount" => $data->total_amount,
            "date_paid" => $database->now(),
            "check_no" => isset($data->check_no) ? $data->check_no : "",
            "check_details" => isset($data->check_details) ? $data->check_details : "",
            "bank" => isset($data->bank) ? $data->bank : "",
            "remarks" => isset($data->remarks) ? $data->remarks : "",
            "status" => "PENDING",
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (ORDER_PAYMENTS, $updateData);
        if($id){

            $_SESSION['image-order-payment-id'] = $data->modifyId;

            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Order Payment Details modified successfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "delete"){
        $updateData = Array (
            "is_deleted" => 1
        );

        $database->where ('id', $data->modifyId);
        $id = $database->update (ORDER_PAYMENTS, $updateData);
        if($id){
            echo json_encode(Array (
                "type" => "success",
                "title" => "Successful!",
                "text" => "Order Payment Deleted succesfully!"
            ));
        }else{
            echo json_encode(Array (
                "type" => "error",
                "title" => "Error!",
                "text" => $database->getLastError()
            ));
        }
    }

    if($data->formAction == "add"){
        saveLog($database,"{$data->formAction} Order Payment: {$loggedUser->franchisee_id}");
    }else{
        saveLog($database,"{$data->formAction} Order Payment ID {$data->modifyId}");
    }
}

//GET METHODS
if(isset($_GET["get"])){
    $primaryKey = 'id';
    $columns = array(
        array( 'db' => 'id', 'dt' => 0 ),
        array( 'db' => 'total_amount',   'dt' => 1 ),
        array( 'db' => 'remarks',   'dt' => 2 ),
        array(  'db' => 'status',   
                'dt' => 3,
                'formatter' => function ($data, $row){

                    return convertStatusColor($data);
                }
            ),
        array(  'db' => 'id',   
                'dt' => 4 ,
                'formatter' => function($data ,$row) {

                    if($row["status"] == "PENDING"){
                        return ' <div class="btn-group dropdown">
                                <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                                <ul role="menu" class="dropdown-menu animated">
                                    <li><a href="#" onclick="modify('.$data.')">Modify</a></li>
                                    <li><a href="#" onclick="viewImage('.$data.')">View Image Receipt</a></li>
                                </ul>
                            </div>';
                    }else{
                        return ' <div class="btn-group dropdown">
                                <button style="color:white;" aria-expanded="false" data-toggle="dropdown" class="btn bg-theme dropdown-toggle waves-effect waves-light" type="button">Take Action <span class="caret"></span></button>
                                <ul role="menu" class="dropdown-menu animated">
                                    <li><a href="#" onclick="viewImage('.$data.')">View Image Receipt</a></li>
                                </ul>
                            </div>';
                    }
                    
                }
            ),
    );
    
    $condition = "is_deleted = 0 and franchise_id = ".$loggedUser->franchisee_id;

    if(isset($_GET['status'])){
        $condition .= " and status = '{$_GET['status']}'";
    }

    echo json_encode(
        SSPCustom::simpleCustom( $_GET, $sqlSSPDetails, ORDER_PAYMENTS , $primaryKey, $columns, $condition )
    );
}

if(isset($_GET["getDetails"])){
    $id = $_GET["getDetails"];

    $database->where ("id", $id);
    $userDB = $database->getOne(ORDER_PAYMENTS);
    echo json_encode($userDB);
}

?>