<?php
require_once("config.php");
require_once("logs.php");

function calculateBillingBalance($database, $data){
        //get franchise information
        $franchise = getFranchise($data["franchise_id"]);

        //check whether the status is approved
        if($data["status"] == "APPROVED"){
            //update total billing balance of the franchise
            $billingBalance = $franchise["billing_balance"] - $data["total_amount"];
    
            $updateFranchiseOrderBalance = array(
                "billing_balance" => $billingBalance,
                "total_amount" => $franchise["order_balance"] + $billingBalance,
                "last_payment_amount" => $data["total_amount"],
                "last_payment_date" => $data["date_paid"]
            );
    
            //update franchise with new values
            $database->where("id", $data["franchise_id"]);
            $database->update(FRANCHISE, $updateFranchiseOrderBalance);
        }
}

function calculateOrderBalance($database, $data){
    //get franchise information
    $franchise = getFranchise($data["franchise_id"]);

    //check whether the status is approved
    if($data["status"] == "APPROVED"){
        //update total order balance of the franchise
        $orderBalance = $franchise["order_balance"] - $data["total_amount"];

        $updateFranchiseOrderBalance = array(
            "order_balance" => $orderBalance,
            "total_amount" => $franchise["billing_balance"] + $orderBalance,
            "last_payment_amount" => $data["total_amount"],
            "last_payment_date" => $data["date_paid"]
        );

        //update franchise with new values
        $database->where("id", $data["franchise_id"]);
        $database->update(FRANCHISE, $updateFranchiseOrderBalance);
    }

}