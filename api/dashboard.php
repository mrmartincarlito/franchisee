<?php
require_once("config.php");
require_once("logs.php");

$loggedUser = json_decode(getLoggedUserDetails($database));


if(isset($_GET['getCards'])){
    $response = array();

    $database->where("id", $loggedUser->franchisee_id);
    $database->where("is_deleted", 0);
    $franchise = $database->getOne(FRANCHISE);

    $response["total_running_balance"] = $franchise["total_amount"] > -1 ? number_format($franchise["total_amount"]) : "(" . number_format(abs($franchise["total_amount"])) . ")";
    $response["billing_balance"] = $franchise["billing_balance"] > -1 ? number_format($franchise["billing_balance"]) : "(" . number_format(abs($franchise["billing_balance"])) . ")";
    $response["order_balance"] = $franchise["order_balance"] > -1 ? number_format($franchise["order_balance"]) : "(" . number_format(abs($franchise["order_balance"])) . ")";
    $response["last_payment_date"] = date("d-m-Y", strtotime($franchise["last_payment_date"]));
    $response["last_payment_amount"] = number_format($franchise["last_payment_amount"]);
    $response["franchise_owner"] = $franchise["name"];
    $response["address"] = $franchise["address"];
    $response["contract_expiration"] = date("d-m-Y", strtotime($franchise["date_contract_expiry"]));

    $branch = getBranch($franchise["branch_id"]);

    $response["branch"] = "[".$branch["branch_code"]."] ".$branch["branch_name"]. " - ".$branch["branch_location"];
    $response["company_address"] = ADDRESS;
    $response["company_contact"] = CONTACT;
    $response["company_email"] = EMAIL;

    echo json_encode($response);
}
