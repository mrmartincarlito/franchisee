<?php
$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

require_once("autoload.php");
require_once ('dbhelper/MysqliDb.php');
require_once("auth.php");
require_once("db.php");
require_once("ssp.class.php");

session_start();
date_default_timezone_set('Asia/Manila');

if(!isset($_COOKIE['username-franchise']) && !preg_match('/login.php/', $actual_link)&& !preg_match('/auth.php/', $actual_link)){
    header('Location: login.php');
}

?>