const PRODUCT_API = './api/products.php'
const ORDER_API = './api/order.php'

$('#orderWizard').wizard({
	onFinish: function() {
		if($("#status").html() == "PENDING"){
			if (!confirm('Are you sure you want to place this order?')) {
				return false
			}

			var orders = []
			var selectedItems = $(`#pre-selected-options`).val()

			for(var i=0; i<selectedItems.length; i++){
				var order = {
					item_id : selectedItems[i],
					price : $("#price_"+selectedItems[i]).val(),
					qty : $("#qty_"+selectedItems[i]).val(),
					total_amount : $("#total_amount_"+selectedItems[i]).val(),
					overall_total : $("#total_amount").html()
				}

				orders.push(order)
			}

			$.blockUI({
				baseZ: 2000
			})

			$.ajax({
				url: ORDER_API,
				type: 'post',
				data: 'saveOrder=' + JSON.stringify(orders),
				processData: false
			})
				.done(data => {
					$.unblockUI()
					
					responseJSON = $.parseJSON(data)
					new PNotify(responseJSON)

					setTimeout(function(){ 
						alert("You can now place your new order"); 
						window.location.href = ".?order_tracker"
					}, 500);
					
				})
				.fail(errorThrown => {
					$.unblockUI()
					console.log('Delete Post Error: ', errorThrown)
					return false
				})
		}else{
			alert("Please settle your existing balance before ordering")
		}
	},

	onNext: function(){
		var selectedItems = $(`#pre-selected-options`).val()
		
		if($("#items_qty").hasClass('active')){
			$.blockUI({
				baseZ: 2000
			})
	
			$.ajax({
				url: ORDER_API + '?itemsLookUp=' + JSON.stringify(selectedItems),
				processData: false
			})
				.done(data => {
					$.unblockUI()
					var json = $.parseJSON(data)
					
					if(json.length == 0){
						$("#itemsTbody").html("<tr><td colspan='5'><center>NO ITEMS SELECTED</center></td></tr>")
					}else{
						var tr = ""
						for(var i=0; i<json.length; i++){
							tr += "<tr>" +
								"<td>"+json[i].id+"</td>" +
								"<td>"+json[i].description+"</td>" +
								"<td style='text-align:right'><input class='form-control' id='price_"+json[i].id+"' type='text' style='text-align:right' readonly value='"+json[i].price+"'></td>" +
								"<td style='text-align:right'><input class='form-control' onkeyup='calculatePrice("+json[i].id+")' id='qty_"+json[i].id+"' type='number' style='text-align:right' value='1'/></td>" +
								"<td style='text-align:right'><input class='form-control' id='total_amount_"+json[i].id+"' type='text' style='text-align:right' readonly value='"+json[i].price+"'></td>" +
							"</tr>"
						}
	
						$("#itemsTbody").html(tr)
					}
				})
				.fail(errorThrown => {
					$.unblockUI()
					console.log('Get Error: ', errorThrown)
					return false
			})
		}

		if($("#verification").hasClass('active')){
			$.blockUI({
				baseZ: 2000
			})
	
			$.ajax({
				url: ORDER_API + '?balanceLookUp',
				processData: false
			})
				.done(data => {
					$.unblockUI()
					var json = $.parseJSON(data)

					$.each(json, function (i, item) {
						$("#" + i).html(item)
					});

					var total_amount = 0;
					for(var i=0; i<selectedItems.length; i++){
						total_amount = total_amount + parseFloat($("#total_amount_"+selectedItems[i]).val().replace(/,/g, ''))
					}

					$("#total_amount").html(formatNumber(total_amount))
				})
				.fail(errorThrown => {
					$.unblockUI()
					console.log('Get Error: ', errorThrown)
					return false
			})
		}
		
	}
});

function calculatePrice(item_id){
	var price = $("#price_"+item_id).val()
	var qty = $("#qty_"+item_id).val()
	
	if(qty == "" || qty == "null" || qty == "undefined" || qty == "0"){
		$("#total_amount_"+item_id).val(1 * price)
	}else{
		$("#total_amount_"+item_id).val(qty * price)
	}
}

$.ajax({
	url: PRODUCT_API + '?getAll',
	processData: false
})
	.done(data => {
		var json = $.parseJSON(data)
		var newVal = ''
		
		newVal += json.map(value => {
			return `<option value='${value.id}'>${value.description} @ Php ${formatNumber(value.price)}</option>`
		})
		$(`#pre-selected-options`).html(newVal)
		$('#pre-selected-options').multiSelect();
		$("#ms-pre-selected-options").css("width", "70%")
	})
	.fail(errorThrown => {
		console.log('Get Error: ', errorThrown)
		return false
})

