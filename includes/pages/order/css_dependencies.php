<?php
  //in order
  $csses = [
    "plugins/bower_components/datatables/jquery.dataTables.min.css", //jquery datatable css
    "plugins/bower_components/datatables/responsive.dataTables.min.css",
    "plugins/bower_components/custom-select/custom-select.css",
    "plugins/bower_components/multiselect/css/multi-select.css",
    "plugins/bower_components/bootstrap-select/bootstrap-select.min.css"
  ];

  foreach($csses as $css){
    echo '<link href="'.$css.'" rel="stylesheet">
    ';
  }
?>