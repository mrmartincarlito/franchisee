const PRODUCT_API = './api/product_information.php'
const ORDER_API = './api/order.php'

myTable = $('#datatable').DataTable({
	processing: true,
	serverSide: true,
	rowReorder: {
		selector: 'td:nth-child(3)'
	},
	responsive: true,
	order: [
		[0, 'desc']
	],
	buttons: [
		{
			extend: 'excel',
			text: 'Export to Excel'
		},
		{

			extend: 'pdf',
			text: 'Export to Pdf',
			orientation: 'portrait',
			filename: 'Product Information',
			paging: true,
			customize: function (doc) {
				doc.content.splice(0, 1);
				var now = new Date();
				var jsDate = now.getDate() + '-' + (now.getMonth() + 1) + '-' + now.getFullYear();
				doc.pageMargins = [20, 60, 20, 30];
				doc.defaultStyle.fontSize = 8;
				doc.styles.tableHeader.fontSize = 8;
				doc['header'] = (function () {
					return {
						columns: [{
							alignment: 'left',
							text: 'Product Information',
							fontSize: 20,
							margin: [20, 20]
						}]
					}
				});
				doc['footer'] = (function () {
					return {
						columns: [{
							alignment: 'left',
							text: 'Created on: ' + jsDate.toString(),
							margin: [10, 10]
						}]
					}
				})
			}
		}
	],
	"language": {
		"lengthMenu": 'Display <select>' +
			'<option value="10">10</option>' +
			'<option value="50">50</option>' +
			'<option value="70">70</option>' +
			'<option value="80">80</option>' +
			'<option value="100">100</option>' +
			'<option value="-1">All</option>' +
			'</select> records'
	},
	ajax: {
		url: PRODUCT_API + '?get',
		complete: function () {
			$(".select2").select2();
			$('#addModal').on('click', function () {
				$('form').trigger('reset')
				$('input[type=checkbox]').prop('checked', false)
				$(`input[name*="modifyId"]`).val('')
				$(`input[name*="formAction"]`).val('add')
				$('#formModal').modal('show')
			})
		}
	}
})

var selectedIds = []

function addToCart(itemId) {
	if (selectedIds.includes(itemId)) {
		new PNotify({ "type": "error", "title": "Error", "text": "Already Added on your cart" })
	} else {
		selectedIds.push(itemId)
		new PNotify({ "type": "success", "title": "Successful", "text": "Successfully added item on your cart", "delay" : 1000 })
		$("#count").html(selectedIds.length)
		//$("#cart_"+itemId).attr("disabled", "true")
	}
}

function checkout() {
	if ($("#count").html() == "0") {
		new PNotify({ "type": "error", "title": "Error", "text": "No Items on your cart" })
		return
	}

	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: ORDER_API,
		type: 'post',
		data: 'previewOrder=' + JSON.stringify(selectedIds),
		processData: false
	})
		.done(data => {
			$.unblockUI()
			$("#checkoutModal").modal("show")
			responseJSON = $.parseJSON(data)
			populateItemInModal(responseJSON)
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Delete Post Error: ', errorThrown)
			return false
		})
}

function calculatePrice(item_id) {
	var price = $("#price_" + item_id).val()
	var qty = $("#qty_" + item_id).val()

	if (qty == "" || qty == "null" || qty == "undefined" || qty == "0") {
		$("#total_amount_" + item_id).val(1 * price)
	} else {
		$("#total_amount_" + item_id).val(qty * price)
	}

	calculateOverAllTotal()
}

function removeItem(itemId) {
	for (var i = 0; i < selectedIds.length; i++) {

		if (selectedIds[i] === itemId) {

			selectedIds.splice(i, 1);
			$("#row_"+itemId).hide()
		}
	}
	$("#count").html(selectedIds.length)
}
function populateItemInModal(json) {
	
	
	var tr = ""
	for (var i = 0; i < json.length; i++) {
		var options = ""

		var initialPrice = json[i].price * json[i].order_count;

		for (var j = 1; j <= 2000; j++) {
			var selected = $("#qty_choice_"+ json[i].id).val();
			var orderCount = j * json[i].order_count;

			if (selected == orderCount) {
				initialPrice = selected * json[i].price;
				options = options + "<option selected>" + orderCount + "</option>"
			} else {
				options = options + "<option>" + orderCount + "</option>"
			}
			
		}

		tr += "<tr id='row_"+json[i].id+"'>" +
			"<td>" + json[i].description + "</td>" +
			"<td style='text-align:right'><input class='form-control' onkeyup='calculatePrice(" + json[i].id + ")' id='price_" + json[i].id + "' type='text' style='text-align:right' readonly value='" + json[i].price + "'></td>" +
			"<td style='text-align:right'><select class='form-control' onchange='calculatePrice(" + json[i].id + ")' id='qty_" + json[i].id + "'>" + options + "</select></td>" +
			"<td style='text-align:right'><input class='form-control' id='total_amount_" + json[i].id + "' type='text' style='text-align:right' readonly value='" + initialPrice + "'></td>" +
			"<td style='text-align:center'><button type='button' onclick='removeItem(" + json[i].id + ")' class='btn btn-danger' title='Remove Item'><i class='fa fa-trash'></i></button></td>" +
			"</tr>"
	}

	$("#checkoutTableBody").html(tr)
	calculateOverAllTotal()
}


function calculateOverAllTotal() {
	var total_amount = 0;

	for (var i = 0; i < selectedIds.length; i++) {
		total_amount = total_amount + parseFloat($("#total_amount_" + selectedIds[i]).val())
	}

	$("#overall_total_amount").html(formatNumber(total_amount))
}

function placeOrder() {
	if (!confirm('Are you sure you want to place this order?')) {
		return false
	}

	var orders = []
	var selectedItems = selectedIds

	for (var i = 0; i < selectedItems.length; i++) {
		var order = {
			item_id: selectedItems[i],
			price: $("#price_" + selectedItems[i]).val(),
			qty: $("#qty_" + selectedItems[i]).val(),
			total_amount: $("#total_amount_" + selectedItems[i]).val(),
			overall_total: $("#total_amount").html()
		}

		orders.push(order)
	}

	$.blockUI({
		baseZ: 2000
	})

	$.ajax({
		url: ORDER_API,
		type: 'post',
		data: 'saveOrder=' + JSON.stringify(orders),
		processData: false
	})
		.done(data => {
			$.unblockUI()

			responseJSON = $.parseJSON(data)
			new PNotify(responseJSON)

			if(responseJSON.type == 'success'){
				setTimeout(function () {
					alert("You can now place your new order");
					window.location.href = ".?order_tracker"
				}, 500);
			}
		})
		.fail(errorThrown => {
			$.unblockUI()
			console.log('Delete Post Error: ', errorThrown)
			return false
		})
}