<?php
  //in order
  $scripts = [
    "plugins/bower_components/datatables/jquery.dataTables.min.js", //jquery datatable js
    "plugins/bower_components/datatables/dataTables.responsive.min.js",
    "plugins/bower_components/datatables/dataTables.buttons.min.js",
    "plugins/bower_components/datatables/jszip.min.js",
    "plugins/bower_components/datatables/pdfmake.min.js",
    "plugins/bower_components/datatables/vfs_fonts.js",
    "plugins/bower_components/datatables/buttons.html5.min.js",
    "plugins/bower_components/datatables/buttons.colVis.min.js",
    "plugins/bower_components/custom-select/custom-select.min.js",
    "plugins/bower_components/bootstrap-select/bootstrap-select.min.js",
    "plugins/bower_components/multiselect/js/jquery.multi-select.js"
  ];

  foreach($scripts as $script){
    echo '<script src="'.$script.'"></script>
    ';
  }
?>