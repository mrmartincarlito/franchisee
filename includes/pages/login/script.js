$(document).ready(() => {
	$('form').submit(function(e) {
		var data = $(this).serializeArray()
		var params = postParams('login', data)

		$.blockUI({
			baseZ: 2000
		})

		$.ajax({
			url: './api/auth.php',
			type: 'post',
			data: 'data=' + params,
			processData: false
		})
			.done(data => {
				
				$.unblockUI()

				try {
					responseJSON = JSON.parse(data)
					new PNotify(responseJSON)
					if (responseJSON.type == 'success') {
						window.location.href = './?order'
					}
				} catch (error) {
					alert("Please check your username and password if correct")
				}
			})
			.fail(errorThrown => {
				$.unblockUI()
				alert("Something was wrong, Please contact your adminstrator")
				console.log('Login POST Response: ', errorThrown)
				console.log(errorThrown)
				return false
			})

		e.preventDefault()
	})
})
