const API = './api/dashboard.php'

// cards numbers
$.ajax({
	url: API + '?getCards',
	processData: false
})
	.done(data => {

		var json = $.parseJSON(data)

		$.each(json, function (i, item) {
			$("#" + i).html(item)
		});
	})
	.fail(errorThrown => {
		console.log('Get Error: ', errorThrown)
		return false
})