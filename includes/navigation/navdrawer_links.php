<?php
    $links = array( 
        array( 
            "name" => "Dashboard", 
            "data-icon" => "a", 
            "link" => "./?dashboard", 
            "child_items" => array(
            ) 
        ),
        array( 
            "name" => "Transaction", 
            "data-icon" => "&#xe01b;", 
            "link" => "|", 
            "child_items" => array(
                array( 
                    "name" => "Place Order", 
                    "data-icon" => ">", 
                    "link" => "./?order", 
                ),
                array( 
                    "name" => "Order Tracker", 
                    "data-icon" => "#", 
                    "link" => "./?order_tracker", 
                ),
                // array( 
                //     "name" => "Return Goods", 
                //     "data-icon" => "&#xe003;", 
                //     "link" => "./?return_goods", 
                // )
            ) 
        ),
        array( 
            "name" => "Billing Payments", 
            "data-icon" => "&#xe019;", 
            "link" => "./?billing_franchising", 
            "linea-type" => "linea-basic",
            "child_items" => array(
                ) 
        ),
        array( 
            "name" => "Order Payments", 
            "data-icon" => "m", 
            "link" => "./?order_payments", 
            "linea-type" => "linea-ecommerce",
            "child_items" => array(
                ) 
        ),
        // array( 
        //     "name" => "Logs", 
        //     "data-icon" => "/", 
        //     "link" => "./?logs", 
        //     "child_items" => array(
        //     ) 
        // ),
    ); 

    //do not meddle with the code below

    $links_HTML = "";
    foreach ($links as $link) {
        $isactive = '';
        if($page_name == strtolower($link['name'])){
            $isactive = 'active';
        }

        $child_links = '';
        $arrow_extra = '';
        if(count($link['child_items']) > 0){
            $child_links = '<ul class="nav nav-second-level">';
            $arrow_extra = '<span class="fa arrow"></span>';
            foreach ($link['child_items'] as $child_item) {
                $child_links .= '
                <li>
                    <a href="'.$child_item['link'].'">
                        <i data-icon="'.$child_item['data-icon'].'" class="linea-icon linea-basic fa-fw"></i><span class="hide-menu">'.$child_item['name'].'</span>
                    </a>
                </li>
                ';
            }
            $child_links .= '</ul>';
        }

        $links_HTML .= '
            <li>
                <a href="'.$link['link'].'" class="waves-effect '.$isactive.'">
                    <i data-icon="'.$link['data-icon'].'" class="linea-icon linea-basic fa-fw"></i>
                    <span class="hide-menu">'.$link['name'].'</span>
                    '.$arrow_extra.'
                </a> 
                    '.$child_links.'
            </li>
        ';
    }
?>
