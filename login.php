<?php 
	require_once("./api/config.php");
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" type="image/png" sizes="16x16" href="plugins/images/thp-logo.png">
  <title>Control Panel Login</title>
  <?php include('./includes/load_on_page_start.php') ?>
</head>

<body>
  <!-- Preloader -->
  <div class="preloader">
    <div class="cssload-speeding-wheel"></div>
  </div>
  <section id="wrapper" class="login-register">
    <div class="login-box">
      <div class="white-box">
        <form class="form-horizontal form-material" id="loginform">
          <a href="javascript:void(0)" class="text-center db"><img src="plugins/images/thp-logo-100px.png" alt="Home" /></a>

          <div class="form-group m-t-40">
            <div class="col-xs-12">
              <input class="form-control" type="text" required="" name="username" placeholder="Username">
            </div>
          </div>
          <div class="form-group">
            <div class="col-xs-12">
              <input class="form-control" type="password" required="" name="password" placeholder="Password">
            </div>
          </div>
          <div class="form-group text-center m-t-20">
            <div class="col-xs-12">
              <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log
                In</button>
            </div>
          </div>
          <!-- <div class="form-group text-center m-t-20">
            <div class="col-xs-12">
              <a href="#">Forgot Password</a>
            </div>
          </div> -->
      </div>
    </div>
  </section>
  <?php include('./includes/load_on_page_end.php') ?>
  <script src="./includes/pages/login/script.js"></script>
</body>

</html>